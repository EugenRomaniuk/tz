<?php
session_start();

if ($_SESSION['user']) {
    header('Location: profile.php');
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Авторизация и регистрация</title>
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>


    <form>
        <label>Логин</label>
        <input id="login" type="text" name="login" class="form-control" placeholder="Введите логин">
        <label>Пароль</label>
        <input id="password" type="password" name="password" class="form-control" placeholder="Введите пароль">
        <button type="submit" class="Login-btn" name="to_login">Войти</button>
        <p>
            Впервые у нас? - <a href="/registration.php">пройдите регистрацию</a>
        </p>
    </form>

    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/main.js"></script>

</body>
</html>