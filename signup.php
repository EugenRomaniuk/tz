<?php
/*ini_set('max_execution_time', 600);увеличил время исполнения скрипта, изначально думал что идёт долгий ответ от userdb.xml, но это не апи и должно работать быстро
проблему так и не решил*/
session_start();
require_once 'connect.php';//подключение к парсеру xml

$full_name = $_POST['full_name'];
$login = $_POST['login'];
$email = $_POST['email'];
$password = $_POST['password'];
$password_confirm = $_POST['password_confirm'];


$rxml=new XMLReader();
while($xml = simplexml_load_file('userdb.xml') && $rxml->login !=='login');
$amountSpent = 0;
while($rxml->login === 'login'){
    $node = new SimpleXMLElement($rxml->XmlTextReader('login'));
    if($node->login == 'login' ){
        $response = [
            "status" => false,
            "message" => "Такой логин уже существует",
            "fields" => ['login']
        ];
    }
    $rxml->next('login');
    echo json_encode($response);
    die();
}



$error_fields = [];

if ($login === '') {
    $error_fields[] = 'login';
}

if ($password === '') {
    $error_fields[] = 'password';
}

if ($full_name === '') {
    $error_fields[] = 'full_name';
}

if ($email === '' || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $error_fields[] = 'email';
}

if ($password_confirm === '') {
    $error_fields[] = 'password_confirm';
}


if (!empty($error_fields)) {
    $response = [
        "status" => false,
        "message" => "Проверьте правильность полей",
        "fields" => $error_fields
    ];

    echo json_encode($response);

    die();
}

if ($password === $password_confirm) {


    $password = md5($password);


    $response = [
        "status" => true,
        "message" => "Регистрация прошла успешно!",
    ];
    echo json_encode($response);


} else {
    $response = [
        "status" => false,
        "message" => "Пароли не совпадают",
    ];
    echo json_encode($response);
}

?>
