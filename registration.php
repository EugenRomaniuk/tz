<?php
    session_start();
    if ($_SESSION['user']) {
        header('Location: profile.php');
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Авторизация и регистрация</title>
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>

    <form action="vendor/signup.php" method="post">
        <label>Придумайте логин для входа в систему</label>
        <input type="text" name="login" placeholder="Введите логин">
        <p class="p">
        Минимальная длина: 6 символов. Используйте только латинские буквы и цифры
</p>
        <label >Придумайте надёжый пароль</label>
        <input type="password" name="password" placeholder="Введите пароль">
        <p class="p">
    Минимальная длина: 6 символов . Обязательно должны содержать цифру, буквы в разных регистрах и спец символ (знаки)
</p>
        <label>Повторите пароль</label>
        <input type="password" name="password_confirm" placeholder="Повторите пароль">
        <p class="p">
        Повторите пароль, чтобы исключить вероятность опечатки.
</p>
        <label>E-mail</label>
        <input type="email" name="email" placeholder="Введите e-mail">
        <p class="p">
    Адрес вашей электронной почты. Будет использоваться для рассылки уведомлений.
</p>
        <label>Имя</label>
        <input type="text" name="name" placeholder="Введите ваше имя">
        <button type="submit">Отправить</button>

        <p>
            У вас уже есть аккаунт? - <a href="/index.php">авторизируйтесь</a>
        </p>

    </form>
    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>